package kz.aitu.project.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@NoArgsConstructor
@Entity
@Table(name = "university_group")
public class Group {
    @Id
    @Column(name = "groupid")
    private long id;
    private String name;
}
