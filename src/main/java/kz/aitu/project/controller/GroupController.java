package kz.aitu.project.controller;

import kz.aitu.project.repository.GroupRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class GroupController {

    private final GroupRepository groupRepository;

    public  GroupController(GroupRepository groupRepository) {
        this.groupRepository = groupRepository;
    }

    @GetMapping("/groups")
    public ResponseEntity<?> getGroups() {
        return ResponseEntity.ok(groupRepository.findAll());
    }
    @GetMapping("/groups/{groupid}")
    public ResponseEntity<?> getGroups(@PathVariable Long groupid) {
        return ResponseEntity.ok(groupRepository.findById(groupid));
    }
}
