insert into university_group(groupId, name) values
(1, 'cs-1901'),
(2, 'cs-1902'),
(3, 'cs-1903'),
(4, 'cs-1904'),
(5, 'cs-1905');

insert into student(name, phone, groupId, id) values
('James', '+7 777 666 1155', 5, 11),
('Kawhi', '+7 776 575 0303', 4, 12),
('Allen', '+7 778 456 0000', 3, 13),
('Paul', '+7 776 123 0777', 2, 14),
('Russel', '+7 778 789 1111', 1, 15);
