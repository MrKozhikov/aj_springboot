drop table if exists student;
create table student(
name varchar(255),
phone varchar(255),
groupId int,
id int primary key
);
alter table student owner to postgres;

drop table if exists university_group;
create table university_group(
name varchar(255),
groupId int primary key
);
alter table student add constraint students_group foreign key(groupId)
references university_group(groupId);
alter table university_group owner to postgres;